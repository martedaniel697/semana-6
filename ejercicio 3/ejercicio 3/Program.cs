﻿using System;

namespace ejercicio_3
{
    class Program
    {
        static void Main(string[] args)
        {
            double num, I, Z, cont = 0;
            string linea;
            Console.Write("Digite el numero: "); linea = Console.ReadLine();
            num = double.parse(linea);
            Z = linea.Length;
            I = Z;
            while (I >= 1)
            {
                I--;
                cont++;
            }
            Console.Write("El numero tiene " + cont + " digitos");
            Console.ReadKey();
        }
    }
}

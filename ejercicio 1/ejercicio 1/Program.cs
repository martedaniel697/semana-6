﻿using System;

namespace ejercicio_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int tabla = 7;
            int num = 0;
            while (num <= 12) 
            {
                Console.WriteLine(tabla + " X " + num + " = " + tabla * num);
                num++;
            }
            Console.ReadKey();
        }
    }
}

﻿using System;

namespace ejercicio_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int n, x, sw, resi;
            string linea;
            x = 2;
            sw = 0;
            Console.Write("Ingrese un numero positivo:");
            linea = Console.ReadLine();
            n = int.Parse(linea);
            while (x < n && sw == 0)
            {
                resi = n % x;
                if (resi == 0)
                {
                    sw = 1;
                }
                else
                {
                    x = x + 1;
                }
            }
            if (sw == 0)
            {
                Console.WriteLine();
                Console.WriteLine("El numero es PRIMO");
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("El numero no es PRIMO");
            }
            Console.ReadKey();
        }
    }
}
